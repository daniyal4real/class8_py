# Ветвление с использованием словарей

# dic = {1: "apple", 2: "banana", 3: "orange", 4: "pineapple"}
#
# if dic.get(2).__contains__("orange"):
#     print("orange was found")
# else:
#     print("fruit is different")


# Generator functions
# The generator function cannot include the return keyword.
# If you include it, then it will terminate the function. The difference between yield
# and return is that yield returns a value and pauses the execution while maintaining
# the internal states, whereas the return statement returns a value and terminates
# the execution of the function.

# def mygenerator():
#     print('First item')
#     yield 10
#
#     print('Second item')
#     yield 20
#
#     print('Last item')
#     yield 30
# gen = mygenerator()
# print(next(gen))


# def get_sequence_upto(x):
#     for i in range(x):
#         yield i
# seq = get_sequence_upto(5)
# print(next(seq))
# print(next(seq))
# print(next(seq))


# Function and recursion:
# def factorial(n):
#     res = 1
#     for i in range(1, n + 1):
#         res *= i
#     return res
#
#
# print(factorial(3))

#
# def factorial_rec(n):
#     if n == 0:
#         return 1
#     else:
#         return n * factorial_rec(n - 1)
#
# print(factorial_rec(3))


# def decorator1(f):
#     def helper():
#         print("Decorating", f.__name__)
#         f()
#
#     return helper
#
# 
# @decorator1
# def my_func():
#     print("inside my_func()")
#
# my_func()
